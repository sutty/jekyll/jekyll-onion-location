# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_render do |site|
  onion = site.config.dig 'onion-location'

  next unless onion

  site.each_site_file do |file|
    next unless file.respond_to? :output
    next if file.output.nil?

    path  = site.config.dig('baseurl') || ''
    path += file.url.start_with?('/') ? '' : '/'

    file.output.sub! '<head>',
                     %(<head><meta http-equiv="onion-location" content="http://#{onion}#{path}#{file.url}"/>)
  end
end
