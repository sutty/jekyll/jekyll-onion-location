# jekyll-onion-location

This plugin adds the [Onion
Location](https://community.torproject.org/onion-services/advanced/onion-location/)
meta attribute to your site, without modifying your theme.


## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-onion-location'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-onion-location

## Usage

Add the plugin to your `_config.yml` and your .onion address:

```yaml
plugins:
- jekyll-onion-location
onion-location: randomstuff.onion
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-onion-location>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-onion-location project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
