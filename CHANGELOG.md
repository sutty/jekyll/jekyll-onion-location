# Changelog

## v0.1.1 - 2020-07-18

* Don't fail if there's no baseurl

## v0.1.0 - 2020-07-18

* Adds `<meta>` for onion locations!
